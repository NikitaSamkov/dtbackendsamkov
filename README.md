# Backend course
## Бот
Ссылка на бота: t.me/SamkovBot
### Команды
![bot commands](img/bot_commands.png)
#### /start
При отправке команды **_/start_** данные пользователя заносятся 
(или обновляются, если уже имеются) в базу данных и бот отвечает 
приветственной фразой  
![/start](img/start.png)
![start result](img/start_result.png)
![start result](img/start_result_big.png)  

#### /set_phone
Команда **_/set_phone_** добавляет номер телефона (или обновляет имеющийся),
переданный в качестве аргумента, к записи пользователя, если таковая 
имеется.  
При успешном добавлении (изменении) номера телефона бот выводит сообщение 
об этом.  
![/set_phone success](img/set_phone_success.png)  
![/set_phone result](img/set_phone_result.png)  
Если данных пользователя в базе нет, тогда бот отправляет 
сообщение, о том, что необходимо сначала отправить команду **_/start_**.  
![/set_phone without /start](img/set_phone_not_registered.png)  
Если введенный номер телефона является некорректным, то бот выводит 
сообщение об этом.  
![incorrect phone](img/set_phone_incorrect.png)  
Кроме того, бот хорошо относится к пробельным и спец символам, при этом
добавляет в базу в едином отформатированном виде.  
![/set_phone with special symbols](img/set_phone_symbols.png)

#### /me
Команда выводит всю информацию, которая записана о пользователе в 
базе данных.  
![/me](img/me.png)  
Если пользователь отправляет команду, и записи о нём в базе нет, бот укажет,
что необходимо сначала отправить команду **_/start_**.  
![not found](img/me_not_found.png)  
Если запись о пользователе всё же есть, но он не заполнил свой номер 
телефона с помощью **_/set_phone_**, бот всё так же не выполняет 
**_/me_**.  
![phone not found](img/me_phone_not_found.png)


## Получение информации через Http запрос
Получение информации о пользователе происходит по пути / api / me / `*id*`,
где **id** - id пользователя  
![http request](img/http.png)