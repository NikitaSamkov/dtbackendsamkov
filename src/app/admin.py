from django.contrib import admin
from .models import BotUser

from app.internal.admin.admin_user import AdminUserAdmin

admin.site.site_title = "Backend course"
admin.site.site_header = "Backend course"


@admin.register(BotUser)
class PersonAdmin(admin.ModelAdmin):
    fields = ('first_name', 'last_name', 'username', 'link', 'phone')
    list_display = ('user_id', 'first_name', 'last_name', 'username', 'link', 'phone')
    list_display_links = ('user_id', 'username')
