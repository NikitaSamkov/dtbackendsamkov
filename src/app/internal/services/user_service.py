from app.models import BotUser
import telegram
import phonenumbers


def add_user(user: telegram.User):
    BotUser.objects.update_or_create(
        user_id=user.id,
        defaults={
            'first_name': user.first_name,
            'last_name': user.last_name,
            'username': f"@{user.username}",
            'link': user.link
        }
    )


def register_phone(user: telegram.User, phone: str):
    if len(phone) > 0 and phone[0] == '8':
        phone = '+7' + phone[1:]
    try:
        parsed = phonenumbers.parse(phone)
    except:
        return False
    if phonenumbers.is_valid_number(parsed):
        get_user_recording(user.id).update(phone=phonenumbers.format_number(parsed, phonenumbers.PhoneNumberFormat.E164))
        return phone
    return False


def check_phone(phone: str):
    return phonenumbers.is_valid_number(phonenumbers.parse(phone))


def get_user_recording(uid: int):
    return BotUser.objects.filter(user_id=uid)


def get_user_information(uid: int):
    rec = get_user_recording(uid).first()
    if rec:
        return {
            'user_id': rec.user_id,
            'first_name': rec.first_name,
            'last_name': rec.last_name,
            'username': rec.username,
            'link': rec.link,
            'phone': rec.phone
        }
    return False
