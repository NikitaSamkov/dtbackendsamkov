from django.urls import path
from app.internal.transport.rest.handlers import get_info


urlpatterns = [
    path('me/<int:uid>', get_info)
]
