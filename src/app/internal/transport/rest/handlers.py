from django.http import JsonResponse
from app.internal.services.user_service import get_user_information


def get_info(request, uid):
    info = get_user_information(uid)
    if not info:
        info = {}
    return JsonResponse(data=info, json_dumps_params={'indent': 2})