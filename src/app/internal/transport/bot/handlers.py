from telegram.ext import CallbackContext
from telegram import Update
from app.internal.services import user_service


def start(update: Update, context: CallbackContext):
    user = update.effective_user
    user_service.add_user(user)
    context.bot.send_message(chat_id=update.effective_chat.id, text=f"Здравствуйте, {user.full_name}!")


def set_phone(update: Update, context: CallbackContext):
    if not user_service.get_user_recording(update.effective_user.id):
        context.bot.send_message(chat_id=update.effective_chat.id, text=f"Для начала работы с ботом, отправьте команду "
                                                                        f"/start")
        return
    args = ' '.join(context.args)
    if user_service.register_phone(update.effective_user, args):
        context.bot.send_message(chat_id=update.effective_chat.id, text=f"Ваш номер {args} успешно добавлен!")
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text=f"Произошла ошибочка: "
                                                                        f"некорректный формат телефона!")


def me(update: Update, context: CallbackContext):
    info = user_service.get_user_information(update.effective_user.id)
    if info:
        if info['phone']:
            context.bot.send_message(chat_id=update.effective_chat.id, text=f"Имя: {info['first_name']}\n"
                                                                            f"Фамилия: {info['last_name']}\n"
                                                                            f"Имя пользователя: {info['username']}\n"
                                                                            f"id: {info['user_id']}\n"
                                                                            f"Ссылка: {info['link']}\n"
                                                                            f"Телефон: {info['phone']}")
        else:
            context.bot.send_message(chat_id=update.effective_chat.id,
                                     text=f"Сначала укажите ваш номер телефона с помощью /set_phone!")
    else:
        context.bot.send_message(chat_id=update.effective_chat.id, text=f"Не найдена запись о вас. Попробуйте /start.")
