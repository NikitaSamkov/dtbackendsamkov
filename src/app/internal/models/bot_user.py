from django.db import models


class BotUser(models.Model):
    user_id = models.IntegerField(primary_key=True)
    first_name = models.CharField(max_length=255)
    last_name = models.CharField(max_length=255)
    username = models.CharField(max_length=255)
    link = models.CharField(max_length=255)
    phone = models.CharField(max_length=255, blank=True)

    def __str__(self):
        return f"{self.user_id}: {self.username}, Phone number: {self.phone if self.phone else 'No phone'}"
