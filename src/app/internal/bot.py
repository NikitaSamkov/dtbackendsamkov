from telegram.ext import Updater
from telegram.ext import Handler


class Bot:
    def __init__(self, TOKEN: str, handlers: list):
        self.updater = Updater(TOKEN, use_context=True)
        self.dispatcher = self.updater.dispatcher
        self.handlers = handlers

    def start(self):
        for handler in self.handlers:
            self.dispatcher.add_handler(handler)
        self.updater.start_polling()
        self.updater.idle()

    def add_handler(self, handler: Handler):
        self.handlers.append(handler)

    def remove_handler(self, handler: Handler):
        self.handlers.remove(handler)
