from django.core.management.base import BaseCommand
from config.settings import BOT_TOKEN
from app.internal.bot import Bot
from app.internal.transport.bot.handlers import *
from telegram.ext import CommandHandler, MessageHandler, Filters


class Command(BaseCommand):
    def handle(self, *args, **options):
        bot = Bot(BOT_TOKEN, [
            CommandHandler('start', start),
            CommandHandler('set_phone', set_phone),
            CommandHandler('me', me)
        ])
        bot.start()
